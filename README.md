# Meant To Be Heard

MeantToBeHeard.com was built responsively using WordPress and the Genesis theme, serving as the clients primary website for information and product ordering.

Together our team scoped out the clients needs during an initial discovery phase, which was used to develop and implement an information architecture plan.

Once our designer had design approval from the client, my role was to build the site with WordPress using the Genesis theme and WooCommerce, as well as ensuring all technical requirements were met.


[Meant To Be Heard site](http://meanttobeheard.com)

[Genesis Theme](https://www.studiopress.com/features)

[WooCommerce](https://woocommerce.com)

